import json
import os

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'letters'))
CONFIG_GENERAL_PATH = '/'.join([ROOT_DIR, 'caracter.json'])


class Caracters:
    def __init__(self):
        pass

    @staticmethod
    def config_categorias_json():
        with open(CONFIG_GENERAL_PATH) as config_file:
            caracters = json.load(config_file)
            CHARACTERS_JSON = caracters["caracteres"]

        return CHARACTERS_JSON

    @staticmethod
    def equivalencia_vector_letters(password=None):
        assert password is not None, "password no puede ser nulo o vacio"
        passw = list(str(password).lower())
        passw = [Caracters.config_categorias_json()["d{k}".format(k=i)] for i in passw]
        passw = [Caracters.xpath_type_list(k) for k in passw]
        return passw

    @staticmethod
    def xpath_type_list(vector=None):
        assert vector is not None, "Vector no puede ser nulo o vacio"
        vector_final = vector
        print(vector[-1:])
        if vector[-1:] in ["Z"]:
            vector_final = \
                f"//div[@class='keyboard__loader']/a[./span/*[name()='svg']/*[name()='g']/*[name()='path' and @d='{str(vector)}']]"
        else:
            vector_final = \
                f"//div[@class='keyboard__loader']/a[./span/*[name()='svg']/*[name()='g']/*[name()='polygon' and @points='{(vector)}']]"

        return vector_final
