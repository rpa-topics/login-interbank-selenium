import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from letters.caracter import Caracters as svgCharacter


class LoginInterbankTest(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_portal_login(self):
        driver = self.driver
        # URL BANCO
        driver.get("https://bancaporinternet.interbank.pe/login")

        # Siguiente linea retorna lista con los xpath de cada letra del password
        svg_vector_list = svgCharacter.equivalencia_vector_letters(password='Jose1243')

        # Elemento input password para hacer click y luego ingresar password
        button0 = "//input[@id='46']"
        button0 = driver.find_element_by_xpath(button0)
        button0.click()
        # time.sleep(4)

        # ingreso de password, iteramos los selectores de la linea  24
        for x in svg_vector_list:
            button_passowrd = driver.find_element_by_xpath(x)
            button_passowrd.click()
            time.sleep(1)

        # continuar con hacer click en le button "continuar"
        # . . .


if __name__ == '__main__':
    unittest.main()
